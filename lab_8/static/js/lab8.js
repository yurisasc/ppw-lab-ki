 // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '176935942892972',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    // implement a function that check login status (getLoginStatus)
    // and run render function below, with a boolean true as a paramater if
    // login status has been connected

    // This is done because when the user opened the web and the user has been logged in,
    // it will automatically display the logged in view
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        render(true);
      }
      else if (response.status === 'not_authorized') {
        render(false);
      }
      else {
        render(false);
      }
    });
  };

  // Facebook call init. default from facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Render function, receive a loginFlag paramater that decide whether
  // render or create a html view for the logged in user or not
  // Modify this method as needed if you feel you need to change the style using
  // Bootstrap's classes or your own implemented CSS
  const render = loginFlag => {
    if (loginFlag) {
      // If the logged in view the one that will be rendered

      // Call getUserData method (see below) that have been implemented by you with a callback function
      // that receive user object as a parameter.
      // This user object is the response from an API facebook call.
      getUserData(user => {
        // Render profile view, post input form, post status button, and logout button
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>'
        );

        // After renderin the above view, get home feed data from the logged in account
        // by calling getUserFeed method which you implement yourself.
        // That method has to receive a callback parameter, which receive a feed object as a response
        // from calling the Facebook API
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render the feed, customize as needed.
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            } else if (value.message) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h1>' + value.message + '</h1>' +
                '</div>'
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="feed">' +
                  '<h2>' + value.story + '</h2>' +
                '</div>'
              );
            }
          });
        });
      });
    } else {
      // The view when not logged in yet
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };

  const facebookLogin = () => {
    // TODO: Implement this method
    // Make sure this method receive a callback function that will call the render function
    // that will render logged in view after successfully logged in,
    // and this function has all needed permissions at the above scope.
    // You can modify facebookLogin function above
    FB.login(function(response){
        console.log(response);
        render(true);
      }, {scope:'public_profile,user_posts,publish_actions,user_about_me,email'})
  };

  const facebookLogout = () => {
    // TODO: Implement this method
    // Make sure this method receive a callback function that will call the render function
    // that will render not logged in view after successfully logged out.
    // You can modify facebookLogout function above
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.logout();
        render(false);
      }
    });
  };

  // TODO: Complete this method
  // This method modify the above getUserData method that receive a callback function called fun
  // and the request user data from the logged in account with all the needed fields in render method,
  // and call that callback function after doing the request and forward the response to that callback function.
  // What does it mean by a callback function?
  const getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me?fields=id,name,about,email,gender,cover,picture.width(168).height(168)', 'GET', function(response){
          console.log(response);
          if (response && !response.error) {
            /* handle the result */
            picture = response.picture.data.url;
            name = response.name;
            userID = response.id;
            fun(response);
          }
          else {
            swal({
              text: "Something went wrong",
              icon: "error"
            });
          }
        });
      }
    });
  };


  const getUserFeed = (fun) => {
    // TODO: Implement this method
    // Make sure this method receive a callback function parameter and do a data request to Home Feed from
    // the logged in account with all the needed fields in render method, and call that callback function
    // after doing the request and forward the response to that callback function.
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        FB.api('/me/posts', 'GET', function(response){
          console.log(response);
          if (response && !response.error) {
            /* handle the result */
            fun(response);
          }
          else {
            swal({
              text: "Something went wrong",
              icon: "error"
            });
          }
        });
      }
    });
  };

  const postFeed = () => {
    // Todo: Implement this method,
    // Make sure this method receive a string message parameter and do a POST request to Feed
    // by going through Facebook API with a string message parameter as a message.
    FB.api('/me/feed', 'POST', {message:message});
    swal("Your post has been posted", {
      icon: "success",
    }).then((ok) => {
      window.location.reload();
    });
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    $('#postInput').val("");
    postFeed(message);
    render(true);
  };

var picture, name, userID;
