from django.test import TestCase
from django.urls import resolve
from .views import index, author
from django.http import HttpRequest

# Create your tests here.
class Lab6UnitTest(TestCase):
    
    def test_index_loads_base(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>\n         Lab 6 By '+author+' \n    </title>', html_response)
