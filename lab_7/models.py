# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Friend(models.Model):
    friend_name = models.CharField(max_length=400)
    npm = models.CharField(max_length=250, unique=True)
    added_at = models.DateField(auto_now_add=True)

    def as_dict(self):
        return {
            "friend_name": self.friend_name,
            "npm": self.npm,
            "id":self.id,
        }
